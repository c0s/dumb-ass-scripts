#!/bin/bash

EFF_UID=`id -u`
EFF_GID=`id -u`

COMMAND=${1:-ls -ls /}
HOSTNAME=${2:-"cos1.docker"}
NAME=${3:-"builduser"}

docker run -h $HOSTNAME -t -i c0sin/bigtop-ignite:repo-tools bash -l -c \
    "groupadd -g $EFF_GID $NAME; useradd -u $EFF_UID -g $EFF_GID  $NAME; su - $NAME -c \"$COMMAND\""
