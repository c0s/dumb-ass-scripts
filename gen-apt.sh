#!/bin/bash

TOP_DIR=${1:-"v1.0"}

for OS in `ls -ld BUILD_OS* | cut -f 2 -d '=' | cut -f 1 -d ',' | grep -v centos | grep -v fedora | uniq `; do 
    rm -rf $TOP_DIR/$OS/apt
    mkdir -p ${TOP_DIR}/$OS/apt/conf
    cat > ${TOP_DIR}/$OS/apt/conf/distributions <<__EOF
Origin: ODPi
Label: ODPi
Suite: stable
Codename: odpi
Version: 1.0
Architectures: i386 amd64 source
Components: contrib
Description: ODPi reference implementation stack
__EOF
    for i in `find BUILD_OS=${OS}* -name \*.changes`; do reprepro -Vb ${TOP_DIR}/$OS/apt include odpi $i; done
done

