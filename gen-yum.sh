#!/bin/bash

TOP_DIR=${1:-"v1.0"}

for OS in `ls -ld BUILD_OS* | cut -f 2 -d '=' | cut -f 1 -d ',' | grep -v debian | grep -v ubuntu | uniq `; do 
  echo $OS;
  mkdir -p ${TOP_DIR}/$OS
  for rpm in `find BUILD_OS=${OS}* -name \*.rpm`; do cp $rpm ${TOP_DIR}/$OS; done
  createrepo -o ${TOP_DIR}/$OS ${TOP_DIR}/$OS
done



